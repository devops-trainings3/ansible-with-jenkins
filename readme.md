## Notes 
This is a demo project where we work with jenkins and ansible . 

### Project Directory 
```yaml 
.
├── ansible
│   ├── inventory.ini
│   ├── playbooks
│   │   ├── install-software-book.yaml
│   │   └── simple-playbook.yaml
│   ├── secrets
│   │   └── mysecret.yaml
│   └── vars
│       └── message.yaml
├── autopush.sh
├── jenkins
│   └── dev.Jenkinsfile
├── readme.md
└── shellscript
    └── helloworld.sh

```